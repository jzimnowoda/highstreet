FROM ruby:2.3.7
RUN mkdir /myapp
WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
#COPY Gemfile.lock /myapp/Gemfile.lock
RUN bundle install
COPY lib /myapp/lib

EXPOSE 3000
CMD ruby /myapp/lib/app.rb -p 3000