require 'sinatra'
require 'uuid'
require_relative 'aws_storage'
require_relative 'models'


configure do
  set :show_exceptions, true

  # Binds to 0.0.0.0 to allow Vagrant port-forwarding from host OS.
  set :bind, '0.0.0.0'
end

get '/' do
  erb :form
end

def is_valid_file_type(image_type)
  return (image_type == 'image/jpeg') || (image_type == 'image/png')
end

post '/api/images/' do

  uuid = UUID.new
  target_filename = uuid.generate(:compact)
  source_path = params[:file][:tempfile]
  url = upload_image(source_path, target_filename)

  data = {
      title: params[:title],
      author: params[:author],
      tags: params[:tags],
      url: url
  }

  unless is_valid_file_type(params[:file][:type])
    halt 412
  end

  image = Image.new(data)
  image.save

  return image.to_json

end

get '/api/images/' do
  Image.all.to_json
end

get '/api/images/:id/' do
  content_type :json
  image ||= Image.get(params[:id]) || halt(404)
  image.to_json
end

put '/api/images/:id/' do
  content_type :json
  image ||= Image.get(params[:id]) || halt(404)
  data = JSON.parse request.body.read
  halt 500 unless image.update(
      tags: data['tags'],
      author: data['author'],
      title: data['title']
  )

  image.to_json

end

delete '/api/images/:id/' do
  content_type :json
  image ||= Image.get(params[:id]) || halt(404)
  halt 500 unless image.destroy
end

