require 'rubygems'
require 'sinatra'
require 'data_mapper' # metagem, requires common plugins too.

DataMapper.setup(:default, "sqlite3://#{Dir.pwd}/highstreet.db")
DataMapper::Model.raise_on_save_failure = true

class Image
  include DataMapper::Resource
  property :id, Serial
  property :title, String, required: true, length: 256
  property :tags, String, required: false, length: 256
  property :author, String, required: false, length: 256
  property :url, String, required: false, length: 2048
end

DataMapper.finalize
# automatically create the post table
Image.auto_upgrade!