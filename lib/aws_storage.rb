require 'aws-sdk'

Aws.config.update(
    endpoint: 'https://highstreet.s3.eu-central-1.amazonaws.com',
    access_key_id: 'AKIA475AUL722KXACFCU',
    secret_access_key: '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',
    force_path_style: true,
    region: 'eu-central-1'
)


def get_bucket
  s3 = Aws::S3::Client.new
  resp = s3.list_objects(bucket: 'highstreet')
  puts resp
  resp.contents.each do |object|
    puts "#{object.key} => #{object.etag}"
  end
  # puts resp.map(&:name)
end


# @param [Object] source_path - an absolute local path
# @param [Object] target_filename - a file name
def upload_image(source_path, target_filename)
  bucket_name = 'images'
  s3 = Aws::S3::Resource.new
  obj = s3.bucket(bucket_name).object(target_filename)
  result = obj.upload_file(source_path)

  if result
    return obj.public_url
  else
    raise 'Unable to store image'
  end
end