ENV['APP_ENV'] = 'test'

require 'app'
require 'test/unit'
require 'rack/test'


class ApiTest < Test::Unit::TestCase
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  def test_it_says_hello_world
    get '/'
    assert last_response.ok?
    assert_equal 'It works', last_response.body
  end

  def test_it_gives_list_of_images
    get '/api/images/'
    assert last_response.ok?
  end

  def test_it_gives_image_details
    get '/api/images/44811d0c-1c1c-4b86-94a8-3609e4e1e7b8/'
    assert last_response.ok?
  end

  def test_updates_image_details
    patch '/api/images/44811d0c-1c1c-4b86-94a8-3609e4e1e7b8/'
    assert last_response.ok?
  end

  def test_remove_imageupdates_image_details
    delete '/api/images/44811d0c-1c1c-4b86-94a8-3609e4e1e7b8/'
    assert last_response.ok?
  end
end
