# API
## Upload image

### Request
POST /api/images/
### Headers
Content-Type: multipart/form-data
```
  {
    "title": "string",
    "tags": "string",
    "author": "string",
    "file": "binary"
  }
```

### Response
HTTP 200
```
{
	id: string
    "title": "string",
    "tags": "string",
    "author": "string",
    "url": "string"
}
```

HTTP 412 - image format not allowed


## Get list of all images
### Request
GET /api/images/
### Headers
Content-Type: application/json

### Response
HTTP 200
```
[]
```

## Get image details
### Request
GET /api/images/<id>/

### Headers
Content-Type: application/json

### Response
HTTP 200
```
{
    url: string,
    tags: list,
    title: string,
    author: string
}
```
HTTP 404 - image not found


## Update image details

### Request
UPDATE /api/images/<id>/

### Headers
Content-Type: application/json

### Response
HTTP 200 - image data updated
```
{
    tags: list,
    title: string,
    author: string
}
```

HTTP 404 - image not found


## Remove image
### Request
DELETE /api/images/<id>/
### Headers
Content-Type: application/json
### Response
HTTP 200 - image removed

HTTP 404 - image not found

# AWS
The aws bucket is called `highstreet `

The bucket policy applies Read-Only Permission to an Anonymous User.
